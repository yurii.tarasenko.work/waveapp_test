//
//  UIViewController+Extensions.swift
//  WaveApp_test
//
//  Created by Tarasenko Jurik on 24.11.2020.
//

import UIKit

extension UIViewController {
    func showAlert(title: String, message: String? = nil, callback: (() -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Okay", style: .default) { _ in
            callback?()
        })
        present(alert, animated: true)
    }
}

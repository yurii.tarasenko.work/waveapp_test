//
//  DetailsViewController.swift
//  WaveApp_test
//
//  Created by Tarasenko Jurik on 24.11.2020.
//

import UIKit

class DetailsViewController: UIViewController {
    
    //MARK: Views
    private let bannerImageView: CustomImageView = {
        let image = CustomImageView()
        image.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        image.layer.cornerRadius = 16.0
        image.layer.masksToBounds = true
        image.contentMode = .scaleAspectFill
        return image
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .center
        label.textColor = #colorLiteral(red: 0.1019607857, green: 0.2784313858, blue: 0.400000006, alpha: 1)
        label.layer.masksToBounds = true
        label.lineBreakMode = .byTruncatingTail
        label.font = UIFont.boldSystemFont(ofSize: 35)
        return label
    }()
    
    private let subTitleTextView: UITextView = {
        let view = UITextView()
        view.isEditable = false
        view.isScrollEnabled = true
        view.textAlignment = .left
        view.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        view.layer.masksToBounds = true
        view.font = UIFont.systemFont(ofSize: 24.0)
        return view
    }()
    
    private let helloButton: UIButton = {
        let button = UIButton()
        button.layer.cornerRadius = 16.0
        button.layer.masksToBounds = true
        button.backgroundColor = .blue
        return button
    }()
    
    //MARK: Property
    var film: Film?
    private let notificationCenter = CustomNotificationCenter.shared
    
    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
        setupUI()
        observeCustomNotificationCenter()
    }
    deinit {
        try? notificationCenter.removeObserver(self)
    }
    
    //MARK: - Func
    private func setupLayout() {
        view.backgroundColor = .white
        view.addSubview(bannerImageView)
        bannerImageView.anchor(top: nil, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 16.0, leftConstant: 0, bottomConstant: 16.0, rightConstant: 0, widthConstant: 0, heightConstant: 380.0)
        bannerImageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        
        view.addSubview(titleLabel)
        titleLabel.anchor(top: bannerImageView.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 16.0, leftConstant: 16.0, bottomConstant: 0, rightConstant: 16.0, widthConstant: 0, heightConstant: 0)
        
        view.addSubview(subTitleTextView)
        subTitleTextView.anchor(top: titleLabel.bottomAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 16.0, bottomConstant: 75.0, rightConstant: 16.0, widthConstant: 0, heightConstant: 0)
        
        view.addSubview(helloButton)
        helloButton.anchor(top: subTitleTextView.bottomAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 10, leftConstant: 24.0, bottomConstant: 24.0, rightConstant: 24.0, widthConstant: 0, heightConstant: 0)
        helloButton.addTarget(self, action: #selector(helloButtonAction), for: .touchUpInside)
        helloButton.setTitle("Say Hello ;)", for: .normal)
    }
    
    private func setupUI() {
        guard let model = film else { return }
        navigationItem.title = model.title
        bannerImageView.loadImageUsingUrlString(urlString: model.posterPath)
        titleLabel.text = model.title
        subTitleTextView.text = model.overview
    }
    
    @objc private func helloButtonAction() {
        try? notificationCenter.postNotification("sayHello", object: "Say Hello ;)")
    }
    
    private func observeCustomNotificationCenter() {
        notificationCenter.addObserver(self, name: "sayHello") { [weak self] (name, object) in
            self?.showAlert(title: "Test Custom Notification Center, and you pressed \n \(object)  😉")
          }
    }
    
}

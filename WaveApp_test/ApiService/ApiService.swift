//
//  ApiService.swift
//  WaveApp_test
//
//  Created by Tarasenko Jurik on 24.11.2020.
//

import UIKit

enum NetworkError: Error {
    case invalidUrl
    case invalidResponse
    case notFound
    case unknown
}

enum HTTPMethod: String {
    case get
}

struct ApiService {
    
    typealias NetworkResult = (Result<Data, NetworkError>) -> Void
    private static let baseUrl = "https://api.themoviedb.org/3/movie/popular?api_key=f910e2224b142497cc05444043cc8aa4&language=en-US&page=1"
    
    public static func get(_ callback: @escaping NetworkResult) {
        request(method: .get, callback: callback)
    }
    
    private static func request(method: HTTPMethod, params: [String: Any]? = nil, callback: @escaping NetworkResult) {
        guard let url = URL(string: baseUrl) else {
            callback(.failure(.invalidUrl))
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        if let params = params,
           let body = try? JSONSerialization.data(withJSONObject: params) {
            request.httpBody = body
        }
        let urlConfig = URLSessionConfiguration.default
        urlConfig.requestCachePolicy = .reloadIgnoringLocalCacheData
        urlConfig.urlCache = nil
        
        let session = URLSession(configuration: urlConfig)
        let task = session.dataTask(with: request) { (data, response, error) in
            if error != nil {
                callback(.failure(.invalidResponse))
                return
            }
            guard let response = response as? HTTPURLResponse,
                  let data = data else {
                callback(.failure(.invalidResponse))
                return
            }
            switch response.statusCode {
            case 200...299:
                callback(.success(data))
            case 404:
                callback(.failure(.notFound))
            default:
                callback(.failure(.unknown))
            }
        }
        task.resume()
    }
}

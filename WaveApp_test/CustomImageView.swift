//
//  CustomImageView.swift
//  WaveApp_test
//
//  Created by Tarasenko Jurik on 24.11.2020.
//

import UIKit
import SDWebImage

final class CustomImageView: UIImageView {
    
    private let baseImageUrl = "https://image.tmdb.org/t/p/original"

    func loadImageUsingUrlString(urlString: String) {
        guard let url = URL(string: baseImageUrl + urlString) else { return }
        image = nil
        self.sd_setImage(with: url)
    }
}

//
//  HomeViewController.swift
//  WaveApp_test
//
//  Created by Tarasenko Jurik on 24.11.2020.
//

import UIKit

class HomeViewController: UIViewController {
    
    //MARK: - Views
    private let tableView: UITableView = {
       let view = UITableView()
        view.separatorStyle = .none
        return view
    }()
    
    //MARK: - Property
    private var films = [Film]()

    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        getData()
    }
    
    //MARK: - Func
    private func setupUI() {
        navigationItem.title = "TOP FILMS"
        tableView.register(FilmCell.self, forCellReuseIdentifier: FilmCell.identifier)
        tableView.rowHeight = 140.0
        tableView.delegate = self
        tableView.dataSource = self
        view.addSubview(tableView)
        tableView.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
    private func getData() {
        ApiService.get { [weak self] result in
            switch result {
            case .success(let data):
                do {
                    guard let self = self,
                          let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any],
                          let filmsDictionary = json["results"] as? [[String: Any]] else {
                        return
                    }
                    let jsonData = try JSONSerialization.data(withJSONObject: filmsDictionary, options: .prettyPrinted)
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    let filmsModels = try decoder.decode([Film].self, from: jsonData)
                    DispatchQueue.main.async {
                        for (index, film) in filmsModels.enumerated() {
                            self.films.append(film)
                            let selectedIndexPath = IndexPath(row: index, section: 0)
                            self.tableView.beginUpdates()
                            self.tableView.insertRows(at: [selectedIndexPath], with: .automatic)
                            self.tableView.endUpdates()
                        }
                    }
                } catch (let error) {
                    print(error.localizedDescription)
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}

//MARK: - UITableViewDelegate
extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailViewController = DetailsViewController()
        detailViewController.film = films[indexPath.row]
        navigationController?.pushViewController(detailViewController, animated: true)
    }
}

//MARK: - UITableViewDataSource
extension HomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return films.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FilmCell.identifier, for: indexPath) as! FilmCell
        cell.selectionStyle = .none
        cell.setupCell(films[indexPath.row])
        return cell
    }
}

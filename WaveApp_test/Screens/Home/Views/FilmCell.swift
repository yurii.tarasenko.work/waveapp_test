//
//  FilmCell.swift
//  WaveApp_test
//
//  Created by Tarasenko Jurik on 24.11.2020.
//

import UIKit

final class FilmCell: UITableViewCell {
    
    private let containerView = UIView()
    
    private let bannerImageView: CustomImageView = {
        let image = CustomImageView()
        image.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        image.layer.cornerRadius = 8
        image.layer.masksToBounds = true
        image.contentMode = .scaleAspectFill
        return image
    }()
    
    private let titleLabel: UILabel = {
       let label = UILabel()
        label.numberOfLines = 2
        label.textAlignment = .left
        label.textColor = #colorLiteral(red: 0.1019607857, green: 0.2784313858, blue: 0.400000006, alpha: 1)
        label.layer.masksToBounds = true
        label.lineBreakMode = .byTruncatingTail
        label.font = UIFont.boldSystemFont(ofSize: 22)
        return label
    }()
    
    private let subTitleLabel: UILabel = {
       let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .left
        label.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        label.layer.masksToBounds = true
        label.lineBreakMode = .byTruncatingTail
        label.font = UIFont.systemFont(ofSize: 16.0)
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayouts()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        titleLabel.text = ""
        subTitleLabel.text = ""
        bannerImageView.image = nil
    }
    
    private func setupLayouts() {
        contentView.addSubview(containerView)
        containerView.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 16.0, bottomConstant: 8, rightConstant: 16.0, widthConstant: 0, heightConstant: 0)
        
        containerView.addSubview(bannerImageView)
        bannerImageView.anchor(top: containerView.topAnchor, left: containerView.leftAnchor, bottom: containerView.bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 8.0, rightConstant: 0, widthConstant: 160.0, heightConstant: 0)
        
        containerView.addSubview(titleLabel)
        titleLabel.anchor(top: containerView.topAnchor, left: bannerImageView.rightAnchor, bottom: nil, right: containerView.rightAnchor, topConstant: 0, leftConstant: 8.0, bottomConstant: 8.0, rightConstant: 8.0, widthConstant: 0, heightConstant: 0)
        
        containerView.addSubview(subTitleLabel)
        subTitleLabel.anchor(top: titleLabel.bottomAnchor, left: bannerImageView.rightAnchor, bottom: containerView.bottomAnchor, right: containerView.rightAnchor, topConstant: 4.0, leftConstant: 8.0, bottomConstant: 8, rightConstant: 8.0, widthConstant: 0, heightConstant: 0)

    }
    
    func setupCell(_ model: Film) {
        bannerImageView.loadImageUsingUrlString(urlString: model.posterPath)
        titleLabel.text = model.title
        subTitleLabel.text = model.overview
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

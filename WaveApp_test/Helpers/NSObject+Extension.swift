//
//  NSObject+Extension.swift
//  WaveApp_test
//
//  Created by Tarasenko Jurik on 24.11.2020.
//

import Foundation

extension NSObject {
    class var identifier: String {
        return String(describing: self)
    }
}

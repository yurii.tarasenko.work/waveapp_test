//
//  Film.swift
//  WaveApp_test
//
//  Created by Tarasenko Jurik on 24.11.2020.
//

import Foundation

struct ApiResult: Codable {
    let result: [Film]
}

struct Film: Codable {

    let id: Int
    let title: String
    let voteAverage: Double
    let voteCount: Int
    let posterPath: String
    let overview: String

}
